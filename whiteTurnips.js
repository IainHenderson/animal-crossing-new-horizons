function HalfDay(minimum, maximum) {
    this.minimum = Math.ceil(minimum);
    this.maximum = Math.ceil(maximum);
    this.compare = function(value) {
        return value == 0 || this.minimum <= value && value <= this.maximum;
    };
}

function range(stop) {
    return [...Array(stop).keys()];
}

function TurnipInput(values) {
    this.base = Math.round(values.shift());
    this.values = range(12).map( index => values.length <= index ? 0 : Math.round(values[index]));;
}

function TurnipResult() {
    this.sellDays = [
        "Monday Morning",
        "Monday Afternoon",
        "Tuesday Morning",
        "Tuesday Afternoon",
        "Wednesday Morning",
        "Wednesday Afternoon",
        "Thursday Morning",
        "Thursday Afternoon",
        "Friday Morning",
        "Friday Afternoon",
        "Saturday Morning",
        "Saturday Afternoon",
    ];
    this.value = 0;
    this.sellDay = this.sellDays[this.sellDays.length - 1];
    this.sellNow = function() {
        if(0 < this.value && this.value <= this.sellDays.length) {
            this.sellDay = this.sellDays[this.value - 1];
        }
    }
}

function evaluateRandomPattern(neeps) {
    // align values with phases
    increasingHalfDay = new HalfDay(neeps.base * .9, neeps.base * 1.4);
    decreaseHalfDays = [
        new HalfDay(neeps.base * .6, neeps.base * .8),
        new HalfDay(neeps.base * .5, neeps.base * .76),
        new HalfDay(neeps.base * .4, neeps.base * .72),
        new HalfDay(neeps.base * .3, neeps.base * .68),
        new HalfDay(neeps.base * .2, neeps.base * .64),
        new HalfDay(neeps.base * .1, neeps.base * .6),
    ];

    result = new TurnipResult();
    // Increasing Phase 1: A random amount between 0 and 6 half days.
    for (halfDay in range(6)){
        if (increasingHalfDay.compare(neeps.values[result])) {
            result.value += 1;
        }
        else {
            break  // Stop when a half day doesn't fall into the increasing range
        }
    }
    increasingPhase1Length = result.value;

    // Decreasing Phase 1: 50% chance of being 2 half days and 50% of being 3 half days.
    for (halfDay in range(3)) {
        if (decreaseHalfDays[halfDay].compare(neeps.values[result])) {
            result.value += 1
        }
        else {
            break;  // Stop when a half day doesn't fall into the increasing range
        }
    }
    decreasingPhase1Length = result.value - increasingPhase1Length;
    if (decreasingPhase1Length < 2) { // Decreasing phase was too short to continue
        return result.value;
    }

    // Increasing Phase 2: Typically a very small increase.
    // Exact duration is found by taking 7 half days and subtracting Increasing Phase 1's length and Increasing Phase 3's Length.
    for (halfDay in range(7 - increasingPhase1Length)) {
        if (increasingHalfDay.compare(neeps.values[result])) {
            result.value += 1;
            result.sellNow()
        }
        else {
            break;  // Stop when a half day doesn't fall into the increasing range
        }
    }

    // Decreasing Phase 2: 5 half days - Decreasing Phase 1 Length.
    for (halfDay in range(5 - decreasingPhase1Length)) {
        if (decreaseHalfDays[halfDay].compare(neeps.values[result])) {
            result.value += 1;
        }
        else {
            return result;  // This is an exact number of days
        }
    }

    // Increasing Phase 3: A random amount between 0 and Increase Phase 2 Length - 1.
    for (halfDay in range(12 - result.value)) {  // what ever remains will be on the increase
        if (increasingHalfDay.compare(neeps.values[result])) {
            result.value += 1;
        }
        else {
            return result;
        }
    }
    return result
}

function evaluateDecreasingPattern(neeps) {
    decreasingHalfDays = [
        new HalfDay(neeps.base * .85, neeps.base * .90),
        new HalfDay(neeps.base * .80, neeps.base * .87),
        new HalfDay(neeps.base * .75, neeps.base * .84),
        new HalfDay(neeps.base * .70, neeps.base * .81),
        new HalfDay(neeps.base * .65, neeps.base * .78),
        new HalfDay(neeps.base * .60, neeps.base * .75),
        new HalfDay(neeps.base * .55, neeps.base * .72),
        new HalfDay(neeps.base * .50, neeps.base * .69),
        new HalfDay(neeps.base * .45, neeps.base * .66),
        new HalfDay(neeps.base * .40, neeps.base * .63),
        new HalfDay(neeps.base * .35, neeps.base * .60),
        new HalfDay(neeps.base * .30, neeps.base * .57),
    ];

    result = new TurnipResult();
    result.value = decreasingHalfDays.findIndex(function(halfDay, index) { return !halfDay.compare(neeps.values[index]) });
    result.sellNow();
    return result;
}

function evaluateSmallSpikePattern(neeps) {
    result = TurnipResult();

    decreasingHalfDays = [
        new HalfDay(neeps.base * .40, neeps.base * .90),
        new HalfDay(neeps.base * .35, neeps.base * .87),
        new HalfDay(neeps.base * .30, neeps.base * .84),
        new HalfDay(neeps.base * .25, neeps.base * .81),
        new HalfDay(neeps.base * .20, neeps.base * .78),
        new HalfDay(neeps.base * .15, neeps.base * .75),
        new HalfDay(neeps.base * .10, neeps.base * .72),
    ];

    increasingHalfDays = [
        new HalfDay(neeps.base * .90, neeps.base * 1.4),
        new HalfDay(neeps.base * .90, neeps.base * 1.4),
        new HalfDay(neeps.base * 1.4, neeps.base * 2.0),
        new HalfDay(neeps.base * 1.4, neeps.base * 2.0),
        new HalfDay(neeps.base * 1.4, neeps.base * 2.0),
    ];

    // Decreasing Phase 1
    for (halfDay in range(7)) {
        if (decreasingHalfDays[halfDay].compare(neeps.values[result])) {
            result.value += 1;
        }
        else {
            break;
        }
    }
    decreasingPhase2Length = 7 - result.value;

    // Increasing Phase
    for (halfDay in range(5)) {
        if (increasingHalfDays[halfDay].compare(neeps.values[result])) {
            result.value += 1;
            if (halfDay == 2) {
                result.sellNow();
            }
        }
        else {
            return result;
        }
    }

    // Decreasing Phase 2
    for (halfDay in range(decreasingPhase2Length)) {
        if (decreasingHalfDays[halfDay].compare(neeps.values[result])) {
            result.value += 1;
        }
        else {
            break;
        }
    }
    return result;
}

function evaluateLargeSpikePattern(neeps) {
    decreasingHalfDays = [
        new HalfDay(neeps.base * .85, neeps.base * .90),
        new HalfDay(neeps.base * .80, neeps.base * .87),
        new HalfDay(neeps.base * .75, neeps.base * .84),
        new HalfDay(neeps.base * .70, neeps.base * .81),
        new HalfDay(neeps.base * .65, neeps.base * .78),
        new HalfDay(neeps.base * .60, neeps.base * .75),
        new HalfDay(neeps.base * .55, neeps.base * .72),
        new HalfDay(neeps.base * .50, neeps.base * .69),
        new HalfDay(neeps.base * .45, neeps.base * .66),
    ]
    increasingHalfDays = [
        new HalfDay(neeps.base * 0.90, neeps.base * 1.40),
        new HalfDay(neeps.base * 1.40, neeps.base * 2.00),
        new HalfDay(neeps.base * 2.00, neeps.base * 6.00),
        new HalfDay(neeps.base * 1.40, neeps.base * 2.00),
        new HalfDay(neeps.base * 0.90, neeps.base * 1.40),
    ];
    randomDecreaseHalfDay = new HalfDay(neeps.base * .40, neeps.base * .90);  // this one is used for the random decreasing phase
    result = TurnipResult();
    // 1. Steady Decreasing Phase (1 - 7 half days)
    for (halfDay in range(8)) {
        if (decreasingHalfDays[halfDay].compare(neeps.values[result])) {
            result.value += 1
        }
        else {
            break;
        }
    }
    if (result.value < 1) {  // Prevent a 0 day long Steady Decrease Phase
        return result;
    }
    // 2. Sharp Increasing Phase (3 half days)
    // 3. Sharp Decreasing Phase (2 half days)
    for (halfDay in range(5)) {
        if (result.value < neeps.values.length) {
            if (increasingHalfDays[halfDay].compare(neeps.values[result])) {
                result.value += 1;
                if (halfDay == 2) {
                    result.sellNow();
                }
            }
            else {
                return result;
            }
        }
    }
    // 4. Random Decreasing Phase (0 - 6 half days)
    if (12 - result.value > 0) {
        for (halfDay in range(12 - result.value)) {
            if (randomDecreaseHalfDay.compare(neeps.values[result])) {
                result.value += 1;
            }
            else {
                break;
            }
        }
    }
    return result;
}

function Possibilities(values) {
    turnips = new TurnipInput(values);
    this.SmallSpike = evaluateSmallSpikePattern(turnips).value / 12.0;
    this.Random = evaluateRandomPattern(turnips).value / 12.0;
    this.LargeSpike = evaluateLargeSpikePattern(turnips).value / 12.0;
    this.Decreasing = evaluateDecreasingPattern(turnips).value / 12.0;
}

function possibleBells(initialValue, patternName) {
    switch (patternName) {
        case 'Decreasing':
            return [initialValue * .85, initialValue * .9];
        case 'LargeSpike':
            return [initialValue * 2, initialValue * 6];
        case 'Random':
            return [initialValue * .9, initialValue * 1.4];
        case 'SmallSpike':
            return [initialValue * 1.4, initialValue * 2];
        default:
            return [initialValue. initialValue];
    }
}
