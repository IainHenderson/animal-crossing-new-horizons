# Stalk Market (White Turnips)
Utility to determine which Stalk Market pattern your island is experiencing

## Python
Call this utility with the purchase price and prices thus far:
`python3 whiteTurnips 98 86 110 102 142`

This example has Sunday's sale price at 98 bells.  On Monday the Nooks were offering 86 and 110 bells.  On Tuesday they were offering 102 and 142

These are the results:
```
Small Spike Pattern: 100%, sell on Tuesday Afternoon for between 138 and 196 bells
Large Spike Pattern: 17%, sell on Saturday Afternoon for between 196 and 588 bells
Decreasing Pattern:  8%, sell on Monday Morning for between 84 and 89 bells
Random Pattern:      0%, sell on Saturday Afternoon for between 89 and 138 bells
```

Small Spike prices are actually "good" for 3 half days, but that's more work.

## Java Script
The JaveScript (ECMAScript?) utility is setup to return an object with probabilities for the possible Stalk Market patterns.  

## Missing Days
Enter 0 as the value for any missing days.  If you didn't record Tuesday morning's prices in the example it would be `python3 whiteTurnips 98 86 110 0 142`

## Sources
This Google Doc supplied the raw patterns, I just made it pretty
https://docs.google.com/document/d/1bSVNpOnH_dKxkAGr718-iqh8s8Z0qQ54L-0mD-lbrXo/edit#